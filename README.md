# element-web-tweaks

Some scripts to improve the experience of using Element Web.

## How to install

The scripts in this repo are
[userscripts](https://openuserjs.org/about/Userscript-Beginners-HOWTO). Some
browsers can install them without an extension, so it's worth clicking the
"Install" links below.

If that does not work, try installing an extension like
[TamperMonkey](https://www.tampermonkey.net/).

These scripts are developed in Firefox using TamperMonkey, so that setup is
the most likely to be successful. Once TamperMonkey or a similar extension
is installed, try clicking the "Install" link below, and it should work.

## Minimal Room List

[Install Minimal Room List](https://gitlab.com/andybalaam/element-web-tweaks/-/raw/main/minimal-room-list.user.js)

Replace Element Web's room list with a more minimal one that prioritises
rooms and chats containing unread messages.

After installing this script, refresh your Element Web page, and wait a few
seconds. Your normal room list should be replaced with a more minimal version.

If something doesn't work, please log an issue in this repo.

![](screenshot-minimal-room-list.png)

[Minimal Room List on openuserjs.org](https://openuserjs.org/scripts/andybalaam/Element_Web_Minimal_Roomlist)
